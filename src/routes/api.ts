
import TestController from "@controllers/TestController";
import express from "express";

let ApiRoute = express.Router();
let testController = new TestController()

ApiRoute.get("/test", (req, res) => testController.index(req, res));

export default ApiRoute