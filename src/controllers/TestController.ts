
import { Request, Response } from "express";
import { HttpResponse } from "@helpers/HttpResponse";
import Todo from "@models/Todo";

export default class TestController{
    constructor() {}

    async index(req: Request, res: Response): Promise<Response> {
        try {
          const filters = req.query.cat_type
            ? { cat_type: req.query.cat_type }
            : {};
          let data = await Todo.find(filters);
          res.status(HttpResponse.OK);
          return res.send({ test: data });
        } catch (error) {
          res.status(HttpResponse.INTERNAL_SERVER_ERROR);
          return res.send({ error: "une erreur c'est produite!" });
        }
      }

}