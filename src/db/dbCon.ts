import "dotenv/config";

import mongoose from "mongoose";
import models from "@models/index";

/**
 *create an instance of database connexion
 *
 * @export
 * @class DBConnexion
 */
export default class DBConnexion {
  
  connection: any = null;
  uri: string = "";
  option: MongoDbOption | null = null;

  constructor() {
    this.connection = null;
    if (process.env.ENV == "production") {
      this.uri = "uri de production"
    } else {
      this.uri = `mongodb://${process.env.HOST}:${process.env.DB_PORT}/${process.env.DB_NAME}`;
    }
    this.option = {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    };
  }

  async createConnection(reset = false) {
    try {
      // get the mongoose connexion
      await mongoose
        .connect(this.uri)
        .then((c) => {
          this.connection = c;
          if (reset) this.initDb();
        })
        .catch((er: any) => {
          throw er;
        });
      console.log("connexion reussit!!!");
    } catch (error) {
      console.error("connexion refused !!!", error);
    }
  }

  initDb(): void {
    models.forEach((model, i) => {
      this.connection.model(model.modelName, model.schema);
    });
  }

  async removeConnection(): Promise<void> {
    if (this.connection == null) return;
    this.connection
      .disconnect()
      .then(() => {
        this.connection = null;
        console.log("connection removed !!");
      })
      .catch((err: any) => {
        console.error("error disconnecting: " + err);
      });
  }
}

type MongoDbOption = {
  useNewUrlParser: boolean;
  useUnifiedTopology: boolean;
};
