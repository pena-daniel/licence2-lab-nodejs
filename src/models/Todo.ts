import { Model, Schema, model } from "mongoose";

const TodoSchema: Schema = new Schema({
    title: {
        type: String,
        required: true,
    },
    isComplete: {
        type: Boolean,
        required: true,
    },
    description: {
        type: String,
        required: false,
    },
    created_at: { type: Date, default: Date.now },
    updated_at: { type: Date, default: Date.now }
}, { collation: { locale: 'en_US', strength: 1 } })


const Todo: Model<any> = model('Todo', TodoSchema)
export default Todo