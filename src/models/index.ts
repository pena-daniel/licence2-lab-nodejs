
import { Model } from "mongoose"
import Todo from "@models/Todo"

const models: Model<unknown>[] = [Todo]

export default models