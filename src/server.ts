import "module-alias/register"
import 'dotenv/config';
import bodyParser from 'body-parser';
import express from 'express';
import DBConnexion from '@db/dbCon';
import ApiRoute from '@routes/api';

// get the db connexion
const db = new DBConnexion()

// connect to the db
db.createConnection(true)

// initiate the express app
const app = express();

// for parsing application/json
app.use(bodyParser.json())

// for parsing application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }))

// register routes
app.use("/admin", ApiRoute)
// lauch the server on the specific prot
app.listen(process.env.PORT || 3001, () => {
    console.log(`server is listening to port http://localhost:${process.env.PORT}`)
});